output public {
  value = {
    services_info = "${module.services_info.info}"
    pitchdeck     = "${module.pitchdeck.info}"
    nightly       = "${module.platform_nightly.info}"
    edge          = "${module.platform_edge.info}"
    demo          = "${module.platform_demo.info}"
    platform      = "${module.platform_production.info}"
  }
}

output domain  {
  value = {
    name         =   "${var.domain["name"]}"
    zone_id      =   "${var.domain["zone_id"]}"
    certificate  =   "${data.aws_acm_certificate.default.arn}"
    api_endpoint =   "api.${var.domain["name"]}"
  }
}

output application  {
  value = "${var.application}"
}
