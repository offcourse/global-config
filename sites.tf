module platform_nightly {
  source      = "./modules/website"
  site_name   = "nightly"
  application = "${var.application}"
  stage       = "${var.stage}"

  domain      = {
    name         = "${var.domain["name"]}"
    zone_id      = "${var.domain["zone_id"]}"
    certificate  = "${data.aws_acm_certificate.default.arn}"
    prefix       = "nightly"
  }
}

module platform_edge {
  source      = "./modules/website"
  site_name   = "edge"
  application = "${var.application}"
  stage       = "${var.stage}"

  domain      = {
    name         = "${var.domain["name"]}"
    zone_id      = "${var.domain["zone_id"]}"
    certificate  = "${data.aws_acm_certificate.default.arn}"
    prefix       = "edge"
  }
}

module platform_demo {
  source          = "./modules/website"
  site_name       = "demo"
  application     = "${var.application}"
  stage           = "${var.stage}"

  domain = {
    name        = "${var.domain["name"]}"
    zone_id     = "${var.domain["zone_id"]}"
    certificate = "${data.aws_acm_certificate.default.arn}"
    prefix      = "demo"
  }
}

module platform_production {
  source          = "./modules/website"
  site_name       = "production"
  application     = "${var.application}"
  stage           = "${var.stage}"

  domain = {
    name        = "${var.domain["name"]}"
    zone_id     = "${var.domain["zone_id"]}"
    certificate = "${data.aws_acm_certificate.default.arn}"
    prefix      = "platform"
  }
}

module pitchdeck {
  source          = "./modules/website"
  site_name       = "pitchdeck"
  application     = "${var.application}"
  stage           = "${var.stage}"

  domain = {
    name        = "${var.domain["name"]}"
    zone_id     = "${var.domain["zone_id"]}"
    certificate = "${data.aws_acm_certificate.default.arn}"
    prefix      = "pitchdeck"
  }
}

module services_info {
  source          = "./modules/website"
  site_name       = "services-info"
  application     = "${var.application}"
  stage           = "${var.stage}"

  domain = {
    name        = "${var.domain["name"]}"
    zone_id     = "${var.domain["zone_id"]}"
    certificate = "${data.aws_acm_certificate.default.arn}"
    prefix      = "services-info"
  }
}
