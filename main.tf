terraform {
  backend "s3" {
    bucket     = "offcourse-remote-state"
    key        = "global-config/terraform.tfstate",
    region     = "us-east-1"
    lock_table = "remote-state-lock"
  }
}

provider aws {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}

resource "aws_api_gateway_domain_name" "api" {
  domain_name = "api.${var.domain["name"]}"
  certificate_arn = "${data.aws_acm_certificate.default.arn}"
}

resource "aws_route53_record" "api" {
  zone_id     = "${var.domain["zone_id"]}"
  name = "${aws_api_gateway_domain_name.api.domain_name}"
  type = "A"

  alias {
    name                   = "${aws_api_gateway_domain_name.api.cloudfront_domain_name}"
    zone_id                = "${aws_api_gateway_domain_name.api.cloudfront_zone_id}"
    evaluate_target_health = true
  }
}


module website {
  source      = "./modules/squarespace"
  domain      = "${var.domain}"
  record_name = "n8w7294k4pycm937gxrw"
}
