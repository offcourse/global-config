variable aws_access_key {}
variable aws_secret_key {}

variable stage {
  default = "global"
}

variable application {
  default = "offcourse"
}

variable domain {
  default = {
    name     = "offcourse.io"
    zone_id  = "Z2QEKD5HDWKH96"
  }
}

data "aws_acm_certificate" "default" {
  domain   = "www.offcourse.io"
  statuses = ["ISSUED"]
}
