resource "aws_route53_record" "root" {
  zone_id = "${var.domain["zone_id"]}"
  name = "${var.record_name}"
  type = "CNAME"
  ttl = "300"
  records = ["verify.squarespace.com"]
}

resource "aws_route53_record" "www" {
  zone_id = "${var.domain["zone_id"]}"
  name = "www"
  type = "CNAME"
  ttl = "300"
  records = ["ext.squarespace.com"]
}

resource "aws_route53_record" "A1" {
  zone_id = "${var.domain["zone_id"]}"
  name    = "${var.domain["name"]}"
  type    = "A"
  ttl     = "300"
  records = ["198.185.159.144", "198.185.159.145", "198.49.23.144", "198.49.23.145"]
}
