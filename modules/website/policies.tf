data "aws_iam_policy_document" "website" {
  statement {
    principals = [
      {
        type        = "AWS"
        identifiers = ["*"]
      },
    ]

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "${aws_s3_bucket.website.arn}/*"
    ]
  }
}

resource "aws_s3_bucket_policy" "website" {
  bucket = "${aws_s3_bucket.website.id}"
  policy = "${data.aws_iam_policy_document.website.json}"
}
