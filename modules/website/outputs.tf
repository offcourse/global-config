output "info" {
  value = {
    endpoint    = "${aws_cloudfront_distribution.default.domain_name}"
    bucket_name = "${aws_s3_bucket.website.id}"
    bucket_arn  = "${aws_s3_bucket.website.arn}"
  }
}
